# GEC Metrics

An exhaustive list of metrics for Grammatical Error Correction. 
Including (for each metric):

- Dockerfile
- requirements.txt
- README.md
- main algorithm named according to the metric's name
- scripts folder, containing additional features or useful algorithms for the main algorithm
- example folder, containing the same example for each metric

# Table of contents

- [GLEU](gleu/README.md)
- [I-measure](i-measure/README.md)
- [MaxMatch](maxmatch/README.md)