# MaxMatch (M² scorer)

This is the scorer for evaluation of grammatical error correction systems. 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License (See [LICENSE](license.md)).

If you are using the NUS M^2 scorer in your work, please include a
citation of the following paper:

Daniel Dahlmeier and Hwee Tou Ng. 2012. Better Evaluation for
Grammatical Error Correction. In Proceedings of the 2012 Conference of
the North American Chapter of the Association for Computational
Linguistics: Human Language Technologies (NAACL 2012).

## Python Version: 2.7

# Parameters
```
usage: maxmatch.py [-h] -r REFERENCE -o OUTPUT [--verbose VERBOSE]
                   [--very-verbose VERY_VERBOSE]
                   [--max_unchanged_words MAX_UNCHANGED_WORDS]
                   [--ignore_whitespace_casing IGNORE_WHITESPACE_CASING]
                   [--beta BETA]

optional arguments:
  -h, --help            show this help message and exit
  -r REFERENCE, --reference REFERENCE
                        enter the path of the expected sentences
  -o OUTPUT, --output OUTPUT
                        enter the path of the outputs
  --verbose VERBOSE     print verbose output
  --very-verbose VERY_VERBOSE
                        print lots of verbose output
  --max_unchanged_words MAX_UNCHANGED_WORDS
                        Maximum unchanged words when extracting edits
  --ignore_whitespace_casing IGNORE_WHITESPACE_CASING
                        Ignore edits that only affect whitespace and casing
  --beta BETA           Set the ratio of recall importance against precision
```

# Docker 

To build the image:
```
docker build . -t maxmatch
```

To run the image:
```
docker run maxmatch:latest [-h] -r REFERENCE -o OUTPUT [--verbose VERBOSE]
                   [--very-verbose VERY_VERBOSE]
                   [--max_unchanged_words MAX_UNCHANGED_WORDS]
                   [--ignore_whitespace_casing IGNORE_WHITESPACE_CASING]
                   [--beta BETA]
```

Example:
`docker run maxmatch:latest -r example/reference.txt -o example/output.txt`

# System output format
The sentences should be in tokenized plain text, sentence-per-line
format.

Format:
```
<tokenized system output for sentence 1>
<tokenized system output for sentence 2>
 ...
```
**Examples of tokenization:**  
 Original  : He said, "We shouldn't go to the place. It'll kill one of us."   
 Tokenized : He said , " We should n't go to the place . It 'll kill one of us . "   

**Note:** Tokenization in the CoNLL-2014 shared task uses NLTK word tokenizer.  

**Sample output:**   
===> system <===
A cat sat on the mat .
The Dog .


# Scorer's gold standard format
SOURCE_GOLD = source sentences (i.e. input to the error correction
system) and the gold annotation in TOKEN offsets (starting from zero). 

**Format:**
```
S <tokenized system output for sentence 1>
A <token start offset> <token end offset>|||<error type>|||<correction1>||<correction2||..||correctionN|||<required>|||<comment>|||<annotator id>
A <token start offset> <token end offset>|||<error type>|||<correction1>||<correction2||..||correctionN|||<required>|||<comment>|||<annotator id>

S <tokenized system output for sentence 2>
A <token start offset> <token end offset>|||<error type>|||<correction1>||<correction2||..||correctionN|||<required>|||<comment>|||<annotator id>
```

**Notes:**   

* Each source sentence should appear on a single line starting with "S "
* Each source sentence is followed by zero or more annotations.
* Each annotation is on a separate line starting with "A ".
* Sentences are separated by one or more empty lines.
* The source sentences need to be tokenized in the same way as the system output.
* Start and end offset for annotations are in token offsets (starting from zero).
* The gold edits can include one or more possible correction strings. Multiple corrections should be separate by '||'.
* The error type, required field, and comment are not used for scoring at the moment. You can put dummy values there.
* The annotator ID is used to identify a distinct annotation set by which system edits will be evaluated.
* Each distinct annotation set, identified by an annotator ID, is an alternative
* If one sentence has multiple annotator IDs, score will be computed for each annotator.
* If one of the multiple annotation alternatives is no edit at all, an edit with type 'noop' or with offsets '-1 -1' must be specified.
* The final score for the sentence will use the set of edits by an annotation set maximizing the score.

Examples can be found in [example](example/).

# References

[Original GitHub repository](https://github.com/nusnlp/m2scorer)