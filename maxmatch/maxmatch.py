import sys
import argparse
from scripts import levenshtein
from getopt import getopt
from scripts.util import paragraphs
from scripts.util import smart_open


def load_annotation(gold_file):
    source_sentences = []
    gold_edits = []
    fgold = smart_open(gold_file, 'r')
    puffer = fgold.read()
    fgold.close()
    # puffer = puffer.decode('utf8')
    for item in paragraphs(puffer.splitlines(True)):
        item = item.splitlines(False)
        sentence = [line[2:].strip() for line in item if line.startswith('S ')]
        assert sentence != []
        annotations = {}
        for line in item[1:]:
            if line.startswith('I ') or line.startswith('S '):
                continue
            assert line.startswith('A ')
            line = line[2:]
            fields = line.split('|||')
            start_offset = int(fields[0].split()[0])
            end_offset = int(fields[0].split()[1])
            etype = fields[1]
            if etype == 'noop':
                start_offset = -1
                end_offset = -1
            corrections =  [c.strip() if c != '-NONE-' else ''
                            for c in fields[2].split('||')]
            # NOTE: start and end are *token* offsets
            original = ' '.join(' '.join(sentence).split()[start_offset:
                                                           end_offset])
            annotator = int(fields[5])
            if annotator not in annotations.keys():
                annotations[annotator] = []
            annotations[annotator].append((start_offset, end_offset, original,
                                           corrections))
        tok_offset = 0
        for this_sentence in sentence:
            tok_offset += len(this_sentence.split())
            source_sentences.append(this_sentence)
            this_edits = {}
            for annotator, annotation in annotations.items():
                this_edits[annotator] = [edit for edit in annotation
                                         if edit[0] <= tok_offset
                                         and edit[1] <= tok_offset
                                         and edit[0] >= 0
                                         and edit[1] >= 0]
            if len(this_edits) == 0:
                this_edits[0] = []
            gold_edits.append(this_edits)
    return (source_sentences, gold_edits)


def print_usage():
    print("Usage: m2scorer.py [OPTIONS] proposed_sentences gold_source\n"
          "where\n"
          "  proposed_sentences   -   system output, sentence per line\n"
          "  source_gold          -   source sentences with gold token edits\n"
          "OPTIONS\n"
          "  -v    --verbose                   -  print verbose output\n"
          "        --very_verbose              -  print lots of verbose "
          "output\n"
          "        --max_unchanged_words N     -  Maximum unchanged words "
          "when extraction edit. Default 2.\n"
          "        --beta B                    -  Beta value for F-measure. "
          "Default 0.5.\n"
          "        --ignore_whitespace_casing  -  Ignore edits that only affect"
          " whitespace and caseing. Default no.")


def main():
    system_file = output
    gold_file = reference

    # load source sentences and gold edits
    source_sentences, gold_edits = load_annotation(gold_file)

    # load system hypotheses
    fin = smart_open(system_file, 'r')
    system_sentences = [line.strip() #line.decode("utf8").strip()
                        for line in fin.readlines()]
    fin.close()

    p, r, f1 = levenshtein.batch_multi_pre_rec_f1(system_sentences,
                                                  source_sentences,
                                                  gold_edits,
                                                  max_unchanged_words,
                                                  beta,
                                                  ignore_whitespace_casing,
                                                  verbose, very_verbose)

    print("Precision   : %.4f" % p)
    print("Recall      : %.4f" % r)
    print("F_%.1f       : %.4f" % (beta, f1))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--reference', help='enter the path of the expected sentences', required=True)
    parser.add_argument('-o', '--output', help='enter the path of the outputs', required=True)
    parser.add_argument('--verbose', default=False, help='print verbose output')
    parser.add_argument('--very-verbose', default=False, help='print lots of verbose output')
    parser.add_argument('--max_unchanged_words', default=2, help='Maximum unchanged words when extracting edits')
    parser.add_argument('--ignore_whitespace_casing', default=False, help='Ignore edits that only affect whitespace and casing')
    parser.add_argument('--beta', default=0.5, help='Set the ratio of recall importance against precision')
    args = parser.parse_args()
    reference, output = args.reference, args.output

    verbose, very_verbose, ignore_whitespace_casing = False, False, False
    max_unchanged_words, beta = 2, 0.5
    if args.verbose:
        verbose = True
    if args.very_verbose:
        very_verbose = True
    if args.max_unchanged_words:
        max_unchanged_words = int(args.max_unchanged_words)
    if args.beta:
        beta = float(args.beta)
    if args.ignore_whitespace_casing:
        ignore_whitespace_casing = True

    main()