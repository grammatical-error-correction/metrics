
# I-measure

This repository contains a Python implementation of the **I-measure**, a metric used for evaluating grammatical error correction systems. A full description of the method can be found in the following paper:

> Mariano Felice and Ted Briscoe. 2015. [**Towards a standard evaluation method for grammatical error detection and correction**](http://www.cl.cam.ac.uk/~mf501/pub/docs/2015-naacl.pdf). In Proceedings of the 2015 Conference of the North American Chapter of the Association for Computational Linguistics: Human Language Technologies (NAACL-HLT 2015), Denver, CO. Association for Computational Linguistics. (To appear)

## Python Version: 2.7

# Parameters

```
usage: i-measure.py [-h] -r REFERENCE -o OUTPUT [--max MAX] [--nomix NOMIX]
                    [--opt OPT] [--b B] [--w W] [--per-sent PER_SENT] [--v V]
                    [--vv VV]

optional arguments:
  -h, --help            show this help message and exit
  -r REFERENCE, --reference REFERENCE
                        enter the path of the expected sentences
  -o OUTPUT, --output OUTPUT
                        enter the path of the outputs
  --max MAX             Maximise scores for the specified metric: *dp*, *dr*,
                        *df*, *dacc*, *dwacc*, *di*, *cp*, *cr*, *cf*, *cacc*,
                        *cwacc* or *ci*. Preceding *d* is for detection, *c*
                        for correction. Available metrics are: *tp* (true
                        positives), *tn* (true negatives), *fp* (false
                        positives), *fn* (false negatives), *p* (precision),
                        *r* (recall), *f* (F measure), *acc* (accuracy),
                        *wacc* (weighted accuracy), *i* (improvement on wacc)
  --nomix NOMIX         Do not mix corrections from different annotators;
                        match the best individual reference instead. By
                        default, the scorer will mix such corrections in order
                        to maximise matches. This option disables default
                        behaviour.
  --opt OPT             Optimise scores at the sentence or corpus level
  --b B                 Specify beta for the F measure
  --w W                 Specify weight of true and false positives for
                        weighted accuracy
  --per-sent PER_SENT   Show individual results for each sentence
  --v V                 Verbose output
  --vv VV               Very verbose output
```

A a table showing the results for the whole input corpus will be printed. Apart from the I-measure, the output will include complementary counts and measures that are useful for comparison and error analysis.

# Docker

To build the image:
```
docker build . -t i-measure
```

To run the image:
```
docker run i-measure:latest [-h] -r REFERENCE -o OUTPUT [--max MAX] [--nomix NOMIX]
                    [--opt OPT] [--b B] [--w W] [--per-sent PER_SENT] [--v V]
                    [--vv VV]
```

Example:
`docker run i-measur:latest -r example/reference.xml -o example/output.txt`

# Gold standard file format

Gold standard references should be provided in XML format according to the following DTD:

```
<!DOCTYPE GOLD-STANDARD [

<!ELEMENT scripts (script)>
<!ELEMENT script (sentence)>
<!ELEMENT sentence (text,error-list)>
<!ELEMENT text (#PCDATA)>
  <!-- Tokenised text of a sentence -->
<!ELEMENT error-list (error)+>
<!ELEMENT error (alt)+>
  <!-- Each of the errors in a sentence -->
<!ELEMENT alt (c)+>
  <!-- Each of alternatives for correcting an error -->
<!ELEMENT c (#PCDATA)>
  <!-- Each individual correction that makes up an alternative -->

<!ATTLIST script id CDATA #REQUIRED>
<!ATTLIST sentence id CDATA #REQUIRED>
<!ATTLIST sentence numann CDATA #REQUIRED>
  <!-- Total number of annotators for a sentence -->
<!ATTLIST error id CDATA #REQUIRED>
<!ATTLIST error req NMTOKEN #REQUIRED>
  <!-- Whether a correction is required for the error (yes/no)-->
<!ATTLIST error type NMTOKEN #REQUIRED>
  <!-- Error type string -->
<!ATTLIST alt ann CDATA #REQUIRED>
  <!-- Id of the annotator providing the alternative -->
<!ATTLIST c end CDATA #REQUIRED>
  <!-- Start position of a token requiring a correction in the original sentence  -->
<!ATTLIST c start CDATA #REQUIRED>
  <!-- End position of a token requiring a correction in the original sentence  -->
]>
```

Check out `example/reference.xml` for an example.

# Converting an .m2 file to XML annotation

You can use `features/m2_to_ixml.py` to convert an existing M² Scorer .m2 file to the XML format used by the I-measure evaluation script. Syntax is as follows:

`python features/m2_to_ixml.py -in:<m2-file> [-out:<xml-file>]`

This conversion script will do some basic automatic merging based on overlapping token positions. More sophisticated merging is required for better results.

# References

[GitHub repository](https://github.com/mfelice/imeasure)