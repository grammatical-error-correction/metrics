These results were described in the ACL 2015 paper:

> [*Ground Truth for Grammatical Error Correction Metrics*](http://www.aclweb.org/anthology/P/P15/P15-2097.pdf)
by Courtney Napoles, Keisuke Sakaguchi, Joel Tetreault, and Matt Post

Please cite this work when using this data or the GLEU metric.

    @InProceedings{napoles-EtAl:2015:ACL-IJCNLP,
      author    = {Napoles, Courtney  and  Sakaguchi, Keisuke  and  Post, Matt  and  Tetreault, Joel},
      title     = {Ground Truth for Grammatical Error Correction Metrics},
      booktitle = {Proceedings of the 53rd Annual Meeting of the Association for Computational Linguistics and the 7th International Joint Conference on Natural Language Processing (Volume 2: Short Papers)},
      month     = {July},
      year      = {2015},
      address   = {Beijing, China},
      publisher = {Association for Computational Linguistics},
      pages     = {588--593},
      url       = {http://www.aclweb.org/anthology/P15-2097}
    }


# GLEU Update

As of May 2, 2016, we have identified a problem with the GLEU metric as the number of references increases. 
To resolve this issue, we made a minor adjustment to the metric so that it no longer has a tunable weight and is reliable using any number of reference sets.
This update to GLEU is reflected in `scripts/compute_gleu` and `scripts/gleu.py`.
The original GLEU scripts can be found in `scripts/original_gleu/`.
We do not recommend using the original GLEU code. The new GLEU should be used instead.

The changes to GLEU and updated results to our ACL 2015 paper are described in the eprint, [*GLEU Without Tuning*](http://arxiv.org/abs/1605.02592).
The citation for the updated metric is

    @Article{napoles2016gleu,
      author    = {Napoles, Courtney  and  Sakaguchi, Keisuke  and  Post, Matt  and  Tetreault, Joel},
      title     = {{GLEU} Without Tuning},
      journal   = {eprint arXiv:1605.02592 [cs.CL]},
      year      = {2016},
      url       = {http://arxiv.org/abs/1605.02592}
    }
