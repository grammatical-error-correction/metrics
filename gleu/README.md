# Ground Truth for Grammatical Error Correction Metrics


This repository contains a python implementation of the GLEU metric
(**G**eneral **L**anguage **E**valuation **U**nderstanding), which
can be used for any monolingual "translation" task. It also contains
human rankings of the CoNLL-14 Shared Task system output as well as
scripts to evaluate the rankings to extract an absolute system
ranking.

## Python Version: 3.10

# Parameters

```
usage: gleu.py [-h] -r [REFERENCE [REFERENCE ...]] -s SOURCE -o [HYPOTHESIS [HYPOTHESIS ...]] [-n N] [-d] [--iter ITER]

optional arguments:
  -h, --help            show this help message and exit
  -r [REFERENCE [REFERENCE ...]], --reference [REFERENCE [REFERENCE ...]]
                        Target language reference sentences. Multiple files for multiple references.
  -s SOURCE, --source SOURCE
                        Source language source sentences
  -o [HYPOTHESIS [HYPOTHESIS ...]], --output [HYPOTHESIS [HYPOTHESIS ...]]
                        Target language hypothesis sentences to evaluate (can be more than one file--the GLEU score of each file will be output separately).
                        Use '-o -' to read hypotheses from stdin.
  -n N                  Maximum order of ngrams
  -d, --debug           Debug; print sentence-level scores
  --iter ITER           the number of iterations to run
```

# Docker

To build the image:
```
docker build . -t gleu
```

To run the image:
```
docker run gleu:latest [-h] -r [REFERENCE [REFERENCE ...]] -s SOURCE -o [HYPOTHESIS [HYPOTHESIS ...]] [-n N] [-d] [--iter ITER]
```

Example:
`docker run gleu:latest -r example/reference.txt -s example/source.txt -o example/output.txt`

# References

[GitHub repository](https://github.com/cnap/gec-ranking)